Poznej FI
Motion Capture Workshop

Co je potřeba pro samotný workshop
 - vše je připraveno, stačí sledovat pokyny instruktorů
 
Co je potřeba, pokud chcete pokračovat doma:
 - stáhnout si tyto materiály z GIT repozitáře, například pomocí nástroje SourceTree
	- www.sourcetreeapp.com
 - Naistalovat herní engine Unity:
	- www.unity3d.com
 - Přístup k online databázi charakterů a animací (je potřeba registrace, zdarma)
	- www.mixamo.com
 - Volitelně - nástroj pro tvorbu vlastních herních charakterů
	- www.adobe.com/cz/products/fuse.html

Jak začít:
 - TBD
