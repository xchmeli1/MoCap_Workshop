﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform HorizontalPivot;

    [SerializeField]
    private Transform VerticalPivot;

    [SerializeField]
    private float rotationSped = 100.0f;

    // Update is called once per frame
    void Update()
    {
        if ( (Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
        {
            HorizontalPivot.Rotate(Vector3.up, Time.deltaTime * rotationSped);
        }
        if ( (Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
        {
            HorizontalPivot.Rotate(Vector3.up, -Time.deltaTime * rotationSped);
        }

        if ((Input.GetKey(KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W)))
        {
            VerticalPivot.Rotate(Vector3.right, Time.deltaTime * rotationSped);
        }
        if ((Input.GetKey(KeyCode.DownArrow)) || (Input.GetKey(KeyCode.S)))
        {
            VerticalPivot.Rotate(Vector3.right, -Time.deltaTime * rotationSped);
        }

        //TODO add vertical camera limits

    }
}
