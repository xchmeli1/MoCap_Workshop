﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DancersController : MonoBehaviour
{
    [SerializeField]
    private Animator dancer;

    private bool playing = true;
    private float lastSpeed;


    public void PlayPause()
    {
        if(playing)
        {
            lastSpeed = dancer.speed;
            dancer.speed = 0.0f;
            playing = false;
        }
        else
        {
            dancer.speed = lastSpeed;
            playing = true;
        }
    }

}
